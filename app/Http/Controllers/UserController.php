<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use Swagger\Annotations as SWG;

class UserController extends Controller
{

    /**
     * @SWG\Get(path="/user",
     *   tags={"user"},
     *   summary="Get all Users",
     *   description="",
     *   operationId="show",
     *   produces={"application/json"},
     *   @SWG\Response(response="default", description="successful operation")
     * )
     */
    /**
     * @SWG\Get(path="/user/{id}",
     *   tags={"user"},
     *   summary="Get all Users",
     *   description="",
     *   operationId="show",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="path",
     *     name="id",
     *     type="integer",
     *     description="User ID",
     *     required=true
     *   ),
     *   @SWG\Response(response="default", description="successful operation")
     * )
     */
    public function show($id = null)
    {
        return empty($id) ? User::all() : User::findOrFail($id);
    }

    /**
     * @SWG\Post(path="/user",
     *   tags={"user"},
     *   summary="Create user",
     *   description="Create new User",
     *   operationId="insert",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="name",
     *     type="string",
     *     description="User name",
     *     required=true
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="address",
     *     type="string",
     *     description="Address",
     *     required=true
     *   ),
     *   @SWG\Response(response="default", description="successful operation")
     * )
     */
    public function insert(Request $request)
    {
        $user = new User;

        $user->name    = $request->name;
        $user->address = $request->address;

        if ($user->save()) {
            return array("message" => "success");
        } else {
            return array("message" => "failed");
        }
    }

    /**
     * @SWG\Put(path="/user/{id}",
     *   tags={"user"},
     *   summary="Update user",
     *   description="",
     *   operationId="update",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="path",
     *     name="id",
     *     type="integer",
     *     description="User ID",
     *     required=true
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="name",
     *     type="string",
     *     description="User name",
     *     required=true
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="address",
     *     type="string",
     *     description="Address",
     *     required=true
     *   ),
     *   @SWG\Response(response="default", description="successful operation")
     * )
     */
    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);

        $user->name    = $request->name;
        $user->address = $request->address;

        if ($user->save()) {
            return array("message" => "success");
        } else {
            return array("message" => "failed");
        }

    }

    /**
     * @SWG\Delete(path="/user/{id}",
     *   tags={"user"},
     *   summary="Delete User",
     *   description="",
     *   operationId="delete",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="User ID",
     *     required=true
     *   ),
     *   @SWG\Response(response="default", description="successful operation")
     * )
     */
    public function delete($id = null)
    {
        if (User::findOrFail($id)->delete()) {
            return array("message" => "success");
        } else {
            return array("message" => "failed");
        }
    }

    //
}
