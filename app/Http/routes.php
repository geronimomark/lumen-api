<?php

/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host="localhost/blog",
 *     basePath="/public",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Swagger Lumen Blog",
 *         description="This is a sample API doc for Lumen",
 *         termsOfService="http://helloreverb.com/terms/",
 *         @SWG\Contact(
 *             email="apiteam@wordnik.com"
 *         ),
 *         @SWG\License(
 *             name="Apache 2.0",
 *             url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *         )
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="Find out more about Swagger",
 *         url="http://swagger.io"
 *     )
 * )
 */


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['namespace' => 'App\Http\Controllers'], function($group){

    $group->get('user',         'UserController@show');
    $group->get('user/{id}',    'UserController@show');

    $group->post('user',        'UserController@insert');

    $group->put('user/{id}',    'UserController@update');

    $group->delete('user/{id}', 'UserController@delete');

});

$app->get('/', function () use ($app) {
    return $app->version();
});
