<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UserTableSeeder');
        for ($i=0; $i < 10; $i++) { 
	        DB::table('users')->insert([
	            'name' => str_random(10),
	            'address' => str_random(10).', Quezon City'
	        ]);
        }

    }
}
